<?php
/**
 * @file
 * Framework for exporting nodes to XML and transforming the output using 
 * templates and FOP. Installation File.
 */

/**
 * Implements hook_schema().
 */
function xml_export_schema() {
  $schema['xml_export'] = array(
    'description' => 'Table where node xml export is saved.',
    'fields' => array(
      'nid' => array(
        'description' => 'The node identifier.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'export' => array(
        'description' => 'The export content XML.',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'begin_tag' => array(
        'description' => 'The export begin tag XML.',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'end_tag' => array(
        'description' => 'The export end tag XML.',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'nodes_referenced' => array(
        'description' => 'The list of referenced nodes',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'users_referenced' => array(
        'description' => 'The list of referenced users',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'exported' => array(
        'description' => 'The Unix timestamp when the export was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'foreign keys' => array(
      'node' => array(
        'table' => 'node',
        'columns' => array('nid' => 'nid'),
      ),
    ),
    'primary key' => array('nid'),
  );
  return $schema;
}
