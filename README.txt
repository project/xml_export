===============================================================================
ABOUT
===============================================================================

The XML export module allows users to export a node and its fields to XML and 
apply XSL transformations. This is useful if you need to generate some PDF 
documents out of your content. This module provides 3 basic functionalities:

1. Export of nodes into a basic and comprehensible XML format
2. Apply XSLT transformations to the generated export
3. Generate PDFs or RTFs using the FOP library

===============================================================================
INSTALLATION
===============================================================================

In order to use FOP processing of XML output, Apache FOP needs to be present in 
xml_export/fop. Download and extract Apache FOP from one of the mirrors in the 
following location :

http://www.apache.org/dyn/closer.cgi/xmlgraphics/fop

Either copy or symling FOP to the xml_export/fop directory. Note that if FOP is 
not present, XML export will work without any FOP processing capabilities. You 
will not be able to produce PDFs or RTFs.
